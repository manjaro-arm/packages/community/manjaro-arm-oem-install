#! /bin/bash

# suppress dmesg output while the script is running
echo 1 > /proc/sys/kernel/printk

#variables
TMPDIR=/var/tmp
SYSTEM=`inxi -M | awk '{print $6}'`
SYSTEMPRO=`inxi -M | awk '{print $7,$8}'`
USERGROUPS=""

#set dialog theme to Manjaro colors
export DIALOGRC="/usr/share/manjaro-arm-oem-install/dialogrc"

# Functions
msg() {
    ALL_OFF="\e[1;0m"
    BOLD="\e[1;1m"
    GREEN="${BOLD}\e[1;32m"
      local mesg=$1; shift
      printf "${GREEN}==>${ALL_OFF}${BOLD} ${mesg}${ALL_OFF}\n" "$@" >&2
 }


create_oem_install() {
    echo "$USER" > $TMPDIR/user
    echo "$PASSWORD" >> $TMPDIR/password
    echo "$PASSWORD" >> $TMPDIR/password
    echo "$ROOTPASSWORD" >> $TMPDIR/rootpassword
    echo "$ROOTPASSWORD" >> $TMPDIR/rootpassword
    msg "Setting root password..."
    passwd root < $TMPDIR/rootpassword 1> /dev/null 2>&1
    msg "Adding user $USER..."
    useradd -m -G wheel,sys,audio,input,video,storage,lp,network,users,power -s /bin/bash $(cat $TMPDIR/user) 1> /dev/null 2>&1
    if [ -d /usr/share/sddm ]; then
    cp /usr/share/sddm/faces/.face.icon /usr/share/sddm/faces/$USER.face.icon
    fi
    usermod -aG $USERGROUPS $(cat $TMPDIR/user) 1> /dev/null 2>&1
    msg "Setting full name to $FULLNAME..."
    chfn -f "$FULLNAME" $(cat $TMPDIR/user) 1> /dev/null 2>&1
    msg "Setting password for $USER..."
    passwd $(cat $TMPDIR/user) < $TMPDIR/password 1> /dev/null 2>&1
    msg "Setting timezone to $TIMEZONE..."
    timedatectl set-timezone $TIMEZONE 1> /dev/null 2>&1
    timedatectl set-ntp true 1> /dev/null 2>&1
    msg "Generating $LOCALE locale..."
    sed -i s/"#$LOCALE"/"$LOCALE"/g /etc/locale.gen 1> /dev/null 2>&1
    locale-gen 1> /dev/null 2>&1
    localectl set-locale $LOCALE 1> /dev/null 2>&1
    if [[ "$SYSTEM" != "Pinebook" ]]; then
    msg "Setting keymap to $KEYMAP..."
    localectl set-keymap $KEYMAP 1> /dev/null 2>&1
    fi
    if [ -f /etc/sway/inputs/default-keyboard ]; then
    sed -i s/"us"/"$KEYMAP"/ /etc/sway/inputs/default-keyboard
		if [[ "$KEYMAP" = "uk" ]]; then
		sed -i s/"uk"/"gb"/ /etc/sway/inputs/default-keyboard
		fi
    fi
    msg "Setting hostname to $HOSTNAME..."
    hostnamectl set-hostname $HOSTNAME 1> /dev/null 2>&1
    msg "Resizing partition..."
    resize-fs 1> /dev/null 2>&1
    #msg "Applying system settings..."
    #systemctl disable systemd-resolved.service 1> /dev/null 2>&1

    msg "Cleaning install for unwanted files..."
    sudo rm -rf /var/log/*

    # Remove temp files on host
    sudo rm -rf $TMPDIR/user $TMPDIR/password $TMPDIR/rootpassword
}

# Kill bootsplash so the script can be seen
echo off > /sys/devices/platform/bootsplash.0/enabled

# Using Dialog to ask for user input for variables
#if [ ! -z "$LOCALE" ]
if [[ "$SYSTEM" != "Pinebook" ]]; then
if [[ "$SYSTEMPRO" = "Pinebook Pro" ]]; then
KEYMAP=$(dialog --clear --title "Finish Manjaro ARM Install" \
	--menu "Is your Pinebook Pro with ISO (uk) or ANSI (us) layout?:" 20 75 10 \
	"uk"       "ISO Layout" \
	"us"       "ANSI Layout" \
	3>&1 1>&2 2>&3 3>&-)
else
let i=0
W=()
while read -r line; do
    let i=$i+1
    W+=($line "")
done < <( localectl list-keymaps )
KEYMAP=$(dialog --clear --title "Finish Manjaro ARM Install" \
	--menu "Choose your keyboard layout!" 20 60 15 \
        "${W[@]}" 3>&1 1>&2 2>&3 3>&- \
	)
fi
fi
# Set the layout for the OEM setup
loadkeys "$KEYMAP"

USER=$(dialog --clear --title "Finish Manjaro ARM Install" \
	--inputbox "Enter desired username:
(Usernames must be all lowercase and first character may not be a number)" 10 90 \
        3>&1 1>&2 2>&3 3>&-)
    if [[ "$USER" =~ [A-Z] ]] || [[ "$USER" =~ ^[0-9] ]] || [[ "$USER" == *['!'@#\$%^\&*()_+]* ]]; then
    clear
    msg "Configuration aborted. Username contained invalid characters. Restarting OEM script..."
    sleep 5 && exec bash /usr/share/manjaro-arm-oem-install/manjaro-arm-oem-install
    fi

if [ ! -z "$USER" ]
then
USERGROUPS=$(dialog --clear --title "Finish Manjaro ARM Install" \
    --inputbox "Enter additional groups for $USER in a comma seperated list: (empty if none)
(default: wheel,sys,audio,input,video,storage,lp,network,users,power)" 10 90 \
        3>&1 1>&2 2>&3 3>&- \
	)
else
	clear
	msg "Configuration aborted. Restarting OEM script..."
	sleep 5 && exec bash /usr/share/manjaro-arm-oem-install/manjaro-arm-oem-install
	#exit 1
fi

FULLNAME=$(dialog --clear --title "Finish Manjaro ARM Install" \
    --inputbox "Enter desired Full Name for $USER:" 8 60 \
        3>&1 1>&2 2>&3 3>&- \
	)

if [ ! -z "$FULLNAME" ]
then
PASSWORD=$(dialog --clear --title "Finish Manjaro ARM Install" \
    --insecure \
	--passwordbox "Enter new Password for $USER:" 8 60 \
        3>&1 1>&2 2>&3 3>&- \
	)
else
	clear
	msg "Configuration aborted. Restarting OEM script..."
	sleep 5 && exec bash /usr/share/manjaro-arm-oem-install/manjaro-arm-oem-install
	#exit 1
fi

if [ ! -z "$PASSWORD" ]
then
CONFIRMPASSWORD=$(dialog --clear --title "Finish Manjaro ARM Install" \
	--insecure --passwordbox "Confirm Password for $USER:" 8 60 \
        3>&1 1>&2 2>&3 3>&- \
	)
else
	clear
	msg "Configuration aborted. Restarting OEM script..."
	sleep 5 && exec bash /usr/share/manjaro-arm-oem-install/manjaro-arm-oem-install
	#exit 1
fi

if [[ "$PASSWORD" != "$CONFIRMPASSWORD" ]]; then
    clear
    msg "User passwords do not match! Restarting OEM script..."
    sleep 5 && exec bash /usr/share/manjaro-arm-oem-install/manjaro-arm-oem-install
    #exit 1
fi

if [ ! -z "$CONFIRMPASSWORD" ]
then
ROOTPASSWORD=$(dialog --clear --title "Finish Manjaro ARM Install" \
    --insecure \
	--passwordbox "Enter new Root Password:" 8 60 \
        3>&1 1>&2 2>&3 3>&- \
	)
else
	clear
	msg "Configuration aborted. Restarting OEM script..."
	sleep 5 && exec bash /usr/share/manjaro-arm-oem-install/manjaro-arm-oem-install
	#exit 1
fi

if [ ! -z "$ROOTPASSWORD" ]
then
CONFIRMROOTPASSWORD=$(dialog --clear --title "Finish Manjaro ARM Install" \
	--insecure --passwordbox "Confirm Root Password:" 8 60 \
        3>&1 1>&2 2>&3 3>&- \
	)
else
	clear
	msg "Configuration aborted. Restarting OEM script..."
	sleep 5 && exec bash /usr/share/manjaro-arm-oem-install/manjaro-arm-oem-install
	#exit 1
fi

if [[ "$ROOTPASSWORD" != "$CONFIRMROOTPASSWORD" ]]; then
    clear
    msg "Root passwords do not match! Restarting OEM script..."
    sleep 5 && exec bash /usr/share/manjaro-arm-oem-install/manjaro-arm-oem-install
    #exit 1
fi

if [ ! -z "$CONFIRMROOTPASSWORD" ]
then
let i=0
W=()
while read -r line; do
    let i=$i+1
    W+=($line "")
done < <( timedatectl list-timezones )
TIMEZONE=$(dialog --clear --title "Finish Manjaro ARM Install" \
    --menu "Choose your timezone!" 20 60 15 \
        "${W[@]}" 3>&1 1>&2 2>&3 3>&- \
	)
else
	clear
	msg "Configuration aborted. Restarting OEM script..."
	sleep 5 && exec bash /usr/share/manjaro-arm-oem-install/manjaro-arm-oem-install
	#exit 1
fi


if [ ! -z "$TIMEZONE" ]
then
let i=0
W=()
while read -r line; do
    let i=$i+1
    W+=($line "")
done < <( cat /etc/locale.gen | grep "UTF-8" | tail -n +2 | awk '{print $1}' | sed -e 's/^#*//' )
LOCALE=$(dialog --clear --title "Finish Manjaro ARM Install" \
	--menu "Choose your locale!" 20 60 15 \
        "${W[@]}" 3>&1 1>&2 2>&3 3>&- \
	)
else
	clear
	msg "Configuration aborted. Restarting OEM script..."
	sleep 5 && exec bash /usr/share/manjaro-arm-oem-install/manjaro-arm-oem-install
	#exit 1
fi

if [ ! -z "$LOCALE" ]
then
HOSTNAME=$(dialog --clear --title "Finish Manjaro ARM Install" \
	--inputbox "Enter desired hostname for this system:" 8 60 \
        3>&1 1>&2 2>&3 3>&- \
	)
else
	clear
	msg "Configuration aborted. Restarting OEM script..."
	sleep 5 && exec bash /usr/share/manjaro-arm-oem-install/manjaro-arm-oem-install
	#exit 1
fi

if [[ "$SYSTEM" != "Pinebook" ]]; then
if [ ! -z "$HOSTNAME" ]
then
dialog --clear --title "Finish Manjaro ARM Install" \
    --yesno "Is the below information correct:
    Username = $USER
    Additional usergroups = $USERGROUPS
    Full Name for $USER = $FULLNAME
    Password for $USER = (password hidden)
    Password for root = (password hidden)
    Timezone = $TIMEZONE
    Locale = $LOCALE
    Keyboard layout = $KEYMAP
    Hostname = $HOSTNAME" 20 60 \
    3>&1 1>&2 2>&3 3>&-
else
    clear
    msg "Configuration aborted. Restarting OEM script..."
    sleep 5 && exec bash /usr/share/manjaro-arm-oem-install/manjaro-arm-oem-install
    #exit 1
fi
else
if [ ! -z "$HOSTNAME" ]
then
dialog --clear --title "Finish Manjaro ARM Install" \
    --yesno "Is the below information correct:
    Username = $USER
    Additional usergroups = $USERGROUPS
    Full Name = $FULLNAME
    Password for $USER = (password hidden)
    Password for root = (password hidden)
    Timezone = $TIMEZONE
    Locale = $LOCALE
    Hostname = $HOSTNAME" 20 60 \
    3>&1 1>&2 2>&3 3>&-
else
    clear
    msg "Configuration aborted. Restarting OEM script..."
    sleep 5 && exec bash /usr/share/manjaro-arm-oem-install/manjaro-arm-oem-install
    #exit 1
fi
fi



response=$?
case $response in
   0) clear; msg "Proceeding....";;
   1) clear; msg "Configuration aborted. Restarting OEM script..."; sleep 5 && exec bash /usr/share/manjaro-arm-oem-install/manjaro-arm-oem-install;;
   255) clear; msg "Configuration aborted. Restarting OEM script..."; sleep 5 && exec bash /usr/share/manjaro-arm-oem-install/manjaro-arm-oem-install;;
esac

create_oem_install

msg "Configuration complete. Cleaning up..."
rm -rf /etc/systemd/system/getty\@tty1.service.d
rm /root/.bash_profile
sed -i s/"PermitRootLogin yes"/"#PermitRootLogin prohibit-password"/g /etc/ssh/sshd_config
sed -i s/"PermitEmptyPasswords yes"/"#PermitEmptyPasswords no"/g /etc/ssh/sshd_config
pacman -Rsn manjaro-arm-oem-install --noconfirm 1> /dev/null 2>&1

if [ -f /usr/bin/sddm ]; then
systemctl enable sddm 1> /dev/null 2>&1
elif [ -f /usr/bin/lightdm ]; then
systemctl enable lightdm 1> /dev/null 2>&1
elif [ -f /usr/bin/gdm ]; then
systemctl enable gdm 1> /dev/null 2>&1
elif [ -f /usr/bin/greetd ]; then
systemctl enable greetd 1> /dev/null 2>&1
fi


secs=$((5))
while [ $secs -gt 0 ]; do
   echo -ne "Rebootin in ... $secs\033[0K\r"
   sleep 1
   : $((secs--))
done
reboot
